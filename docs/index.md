# IDF Gestão

## Conteúdo de Ajuda

- **Manuais:**
	- Gerenciando usuários  
		- [Editando Usuários](./configuracoes/usuarios.md)
		- [Editando Papéis](./configuracoes/papeis.md)
		- [Papéis pré-definidos](./configuracoes/papeis-pre-definidos.md)
	- [Obrigações do usuário de ECF](./manuais/obrigacoes-ecf.md)
	- [Atualizando NCM/CEST de produtos](./manuais/ncm-cest.md)

## Sobre o IDF Gestão

### Automação Comercial 100% Cloud, 0% Stress

Administre as operações da sua empresa com agilidade e mobilidade. Completo desde a compra, passando pela venda, finanças, até a parte fiscal (SPED). Gerencie o seu negócio da sua casa ou de onde estiver com segurança e privacidade utilizando nossa solução online.

### PDV com PAF-ECF

Rápido e confiável. Operação ininterrupta de alto desempenho com modo offline transparente ao usuário e totalmente integrado com nossa solução online. Ideal para bares, restaurantes, supermercados e cinemas.

### Sistema de pedidos e gestão de salão

Sistema de Pedidos compatível com dispositivos touchscreen como celulares, tablets e monitores touchscreen. Aliado à uma integração inteligente com impressora localizada no ambiente de produção, torna-se a ferramenta ideal para facilitar o trabalho dos garçons do seu estabelecimento e garantir que os pedidos dos seu clientes sejam lançados corretamente e entregue com agilidade e precisão.

### Sistema de Check-in de clientes

Faça o check-in dos seus clientes na entrada do estabelecimento através de um celula, tablet ou computador. Mantenha os dados do seus lientes sempre a mão e diretamente relacionado com o consumo deles no seu estabelecimento.

