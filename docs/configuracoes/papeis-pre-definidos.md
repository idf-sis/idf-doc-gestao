# Papéis pré definidos

Para facilitar a gestão de permissões de usuários o IDF Gestão vem pré configurado com um conjunto de papéis de usuários. Cada um com suas permissões pré definidas de acordo com o objetivo do papel.

**Observação:** Você poderá renomear, alterar as permissões, remover ou criar novos papeis a qualquer momento. _Veja [Editando Papeis](./papeis.md) para mais detalhes._

Os seguintes papéis são pré-definidos na instalação do IDF Gestão:

## Administrador

Usuário administrador da empresa. Possuí permissões relacionadas a configuração da empresa e ao acompanhamento dos resultados das operações realizadas no estabelecimento. Incluindo também permissões para gerenciar usuários.

---

## Gestor de usuários

Usuário com permissões para gerenciar usuários e seus papéis dentro da empresa. Incluindo também permissões para alterar os papéis cadastrados.

---

## Supervisor financeiro

Usuário com permissões totais nas funcionalidades relacionadas ao módulo financeiro.

---

## Assistente financeiro

Usuário com permissões nas funcionalidades relacionadas ao módulo financeiro exceto algumas funcionalidades de remoção de dados.

---

## Consultor Fiscal

Usuário com permissões relacionadas as configurações e operações fiscais do sistema

---

## Supervisor de restaurante

Usuário com permissões totais nas funcionalidades relacionadas ao módulo Administrativo.

---

## Assistente de restaurante

Usuário com permissões básicas nas funcionalidades relacionadas ao módulo Administrativo excluíndo algumas funcionalidades de remoção de dados.

---

## Chefe de cozinha

Usuário com permissões relacionadas a gestão da ficha-técnica dos produtos

---

## Supervisor de Caixa

Usuário com permissões totais nos Pontos de Venda incluíndo cancelamento de vendas

---

## Operador de Caixa

Usuário com permissões básicas nos Pontos de Venda

---

## Recepcionista

Usuário com permissões relacionadas ao controle de check-in e cadastro de novos clientes

---

## Supervisor de cinema

Usuário com permissões totais as funcionalidades do módulo de Cinema

---

## Assistente de cinema

Usuário com permissões básicas as funcionalidades do módulo de Cinema excluíndo algumas funcionalidades de remoção de dados

---

## Supervisor de eventos

Usuário com permissões totais as funcionalidades do módulo de Eventos

---

## Assistente de eventos

Usuário com permissões básicas as funcionalidades do módulo de Eventos excluíndo algumas funcionalidades de remoção de dados
