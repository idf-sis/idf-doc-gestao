# Papéis de usuários

O Papel representa uma função exercida dentro do estabelecimento. Cada papel possuí um conjunto determinado de permissões de acesso relacionadas a ele. Os usuários executores de um papel terão todas as permissões relacionadas aquele papel.

Um usuário recem criado não possuirá nenhum papel e por isso não terá permissão de acesso a nenhuma funcionalidade do sistema.

As permissões de acesso poderão estar relacionadas a outras aplicações além do IDF Gestão. São elas:

 - **IDF Gestão**: Sistema de Gestão on-line
 - **IDF Retaguarda**: Sistema de Retaguarda local do estabelecimento
 - **IDF PDV**: Sistema de caixa executado localmente em cada um dos Pontos de Venda do estabelecimento
 - **Global**: Permissões que envolvem todos três sistemas acima

---

## Papéis pré definidos

Para facilitar a gestão de permissões de usuários o IDF Gestão vem pré configurado com um conjunto de papéis de usuários. Cada um com suas permissões pré definidas de acordo com o objetivo do papel. _Veja [Papéis pré-definidos](./papeis-pre-definidos.md) para mais detalhes._

Você poderá renomear, alterar as permissões, remover ou criar novos papeis a qualquer momento. _Veja [Editando Papeis](./papeis.md) para mais detalhes._

---

## Listando papéis existentes

1. Acesse módulo: **"Configurações > Controle Acesso > Papéis"**
2. Será exibida a lista de papéis existentes
3. Clique em um dos papéis listados para exibir as permissões relacionadas ao papel selecionado.

> Essa funcionalidade requer as permissões:  
> 
> - Controle Acesso > Visualizar papeis

---

## Renomeando papéis existentes

1. Acesse módulo: **"Configurações > Controle Acesso > Papéis"**
2. Será exibida a lista de papéis existentes
3. Clique no _**Lápis**_ a esquerda de um dos papéis para renomeá-lo

> Essa funcionalidade requer as permissões:  
> 
> - Controle Acesso > Visualizar papeis
> - Controle Acesso > Editar papeis

---

## Alterando permissões dos papéis existentes

1. Acesse módulo: **"Configurações > Controle Acesso > Papéis"**
2. Será exibida a lista de papéis existentes
3. Clique no nome do papel para exibir as permissões relacionadas ao papel
5. Será exibido ao lado da lista de papéis um quadro com as permissões do papel por aplicação
6. Marque/desmarque as permissões listadas para alterar as permissões do papel


> Essa funcionalidade requer as permissões:  
> 
> - Controle Acesso > Visualizar papeis
> - Controle Acesso > Editar papeis

---

## Listando os usuários de um papel

1. Acesse módulo: **"Configurações > Controle Acesso > Papéis e usuários"**
2. Selecione o papel para ver os seus usuários
3. Será exibida uma lista de usuários que executam o papel selecionado

> Essa funcionalidade requer as permissões:  
> 
> - Controle Acesso > Visualizar papeis
> - Controle Acesso > Visualizar papeis de usuários
