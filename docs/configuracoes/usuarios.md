# Editando usuários

Utilize o módulo de Usuários do IDF Gestão para cadastrar, alterar e bloquear usuários no sistema.

**RECOMENDAÇÕES DE SEGURANÇA:**

- **Duas pessoas diferentes não devem usar o mesmo usuário**: _O sistema mantém log das operações realizadas por cada usuário e se um mesmo usuário for usado por mais de uma pessoa você poderá a rastreabilidade de quem fez a ação._
- **Lembre-se sempre de boquear usuários desligados da empresa**: _É uma questão de segurança bloquear imediatamente os usuários desligados do estabelecimento para que eles percam acesso aos seus dados._
- **Use o e-mail real do usuário**: _O e-mail de cadastro do usuário é utilizado para que ele consiga recuperar a senha no sistema. Sem ele o usuário não conseguirá mais acessar o sistema caso esqueça a sua senha._

---

## Listando usuários existentes

1. Acesso o módulo: "**Configurações > Usuarios"**
2. Será exibida a lista de usuários cadastrados no sistema que não estão bloqueados  
_Observação:_ Para exibir os usuários bloqueados basta marcar a opção **"Exibir usuários bloqueados"**


> Essa funcionalidade requer as permissões:  
> 
> - Controle Acesso > Visualizar usuários  
> - Controle Acesso > Visualizar papéis de usuários

---

## Visualizando usuários existentes

1. Acesso o módulo: **"Configurações > Usuarios"**
2. Selecionar o usuário desejado na listagem de usuários exibida


> Essa funcionalidade requer as permissões:
> 
> - Controle Acesso > Visualizar usuários
> - Controle Acesso > Visualizar papéis de usuários


---

## Cadastrando novos usuários

1. Acesso o módulo: **"Configurações > Usuarios"**
2. Clique em: **"Cadastrar novo usuário"**
3. Preencha os dados do novo usuário e clique no botão **"Salvar"**

**Observações:**

- Após criar o usuário ele não terá acesso nas funcionalidades do sistema enquanto o papel do usuário não for definido.  
_Veja [Alterando papéis de usuários](#alterando-papeis-de-usuarios) para mais detalhes._
- Uma vez criado o usuário não poderá mais ser removido, apenas bloqueado. _Veja [bloqueando-usuarios](#bloqueando-usuarios) para mais detalhes._
- O campo **"PIN"** é necessário para que o usuários que acessem o sistema de _IDF Pedidos_ ou autorizem outros usuários a realizar operações de cancelamento de venda. Caso o usuário não realize essas ações este campo poderá ficar em branco.


> Essa funcionalidade requer as permissões:
> 
> - Controle Acesso > Visualizar usuários
> - Controle Acesso > Visualizar papéis de usuários
> - Controle Acesso > Editar Usuários

---

## Alterando usuários existentes

1. Acesso o módulo: **"Configurações > Usuarios"**
2. Selecionar o usuário desejado na listagem de usuários exibida
3. Clique no **"Lápis"** exibido ao lado do nome do usuário
4. Edite as informações desejadas e clique no botão **"Salvar"**

"**Observação:"** Não é permitido alterar o _Login_ de um usuário já cadastrado


> Essa funcionalidade requer as permissões:
> 
> - Controle Acesso > Visualizar usuários
> - Controle Acesso > Visualizar papéis de usuários
> - Controle Acesso > Editar Usuários

---

## Bloqueando usuários

Para permitir que o histórico de ações no sistema não seja perdido um usuário não poderá ser deletado após ter sido criado. Dessa forma, caso o usuário tenha se desligado da empresa é necessário realizar o bloqueio do mesmo. Para bloquear um usuário:

1. Acesso o módulo: **"Configurações > Usuarios"**
2. Selecionar o usuário desejado na listagem de usuários exibida
3. Marque a opção **"Bloqueado: Sim"**

> Essa funcionalidade requer as permissões:
> 
> - Controle Acesso > Visualizar usuários
> - Controle Acesso > Visualizar papéis de usuários
> - Controle Acesso > Editar Usuários

---

## Alterando papéis de usuários

O Papel de um usuário representa a função que ele exerce dentro do estabelecimento. É necessário definir os papeis do usuário dentro do sistema para que ele ganhe permissões de acesso às funcionalidades do sistema. Cada papel dará ao usuário um conjunto determinado de permissões de acesso. E cada usuário poderá ter mais de um papel acumulando as permissões desses papeis.

Os papeis e as permissões de acesso de um usuário são listados na tela de detalhes do usuário. Nessa mesma tela você poderá incluir ou remover papeis do usuário clicando em **"Incluir papel"** ou no **"X"** localizado a direita do nome do papel incluído.

Um usuário recem criado não possuirá nenhum papel e por isso não terá permissão de acesso a nenhuma funcionalidade do sistema.

As permissões de acesso fornecidas pelo papel poderão estar relacionadas a outras aplicações além do IDF Gestão. Dessa forma, ao alterar o papel de um usuário no IDF Gestão essa alteração também irá se replciar nas outras aplicações. As aplicações que poderão ter permissões alteradas são:

 - **IDF Gestão**: Sistema de Gestão on-line
 - **IDF Retaguarda**: Sistema de Retaguarda local do estabelecimento
 - **IDF PDV**: Sistema de caixa executado localmente em cada um dos Pontos de Venda do estabelecimento
 - **Global**: Permissões que envolvem todos três sistemas acima

> Essa funcionalidade requer as permissões:
> 
> - Controle Acesso > Visualizar usuários
> - Controle Acesso > Visualizar papéis de usuários
> - Controle Acesso > Editar Usuários
> - Controle Acesso > Alterar papeis de usuários


### Papéis pré-definidos

Para facilitar a gestão de permissões de usuários o IDF Gestão vem pré configurado com um conjunto de papéis de usuários. Cada um com suas permissões pré definidas de acordo com o objetivo do papel. _Veja [Papéis pré-definidos](./papeis-pre-definidos.md) para mais detalhes._

**Observação:** Você poderá renomear, alterar as permissões, remover ou criar novos papeis a qualquer momento. _Veja [Editando Papeis](./papeis.md) para mais detalhes._
