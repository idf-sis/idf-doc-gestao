# Código NCM e Código CEST

## Legislação pertinente

A partir de **1º de Outubro de 2016** passa a ser obrigatório o preenchimento dos campos CEST e NCM e impressão dessa informação junto a descrição dos itens nos cupons fiscais emitidos por equipamentos ECF em Minas Gerais.

O CEST, instituído pelo Convênio ICMS 92/15, é o código que identifica a mercadoria passível de sujeição aos regimes de substituição tributária e de antecipação do recolhimento do imposto, relativos às operações subsequentes. Ele deve ser usado em documentos fiscais conforme especificado no Convênio ICMS 92/15. Sendo que seu uso passa a ser obrigatório a partir de 10 de agosto de 2016 conforme Convênio ICMS 16, de 24 de março de 2016.

NCM é um código de oito dígitos estabelecido pelo Governo Brasileiro para identificar a natureza das mercadorias. Qualquer mercadoria, importada ou comprada no Brasil, deve ter um código NCM na sua documentação legal (nota fiscal, livros legais, etc.), cujo objetivo é classificar os itens de acordo com regulamentos do Mercosul. A NCM foi adotada em janeiro de 1995 pela Argentina, Brasil, Paraguai e Uruguai e tem como base o SH (Sistema Harmonizado de Designação e Codificação de Mercadorias). Por esse motivo existe a sigla NCM/SH.

> **REFERÊNCIAS:**
> 
>  - [Prazo para preenchimento do CEST](http://www.fazenda.mg.gov.br/noticias/2016_03_30_CEST_Prorrogacao.html)
>  - [CONVÊNIO ICMS 25, DE 8 DE ABRIL DE 2016](https://www.confaz.fazenda.gov.br/legislacao/convenios/2016/cv025_16)
>  - [CONVÊNIO ICMS 92, DE 20 DE AGOSTO DE 2015](https://www.confaz.fazenda.gov.br/legislacao/convenios/2015/CV092_15)
>  - [SEF/MG](http://www.fazenda.mg.gov.br/)

---

## O que é o código CEST

CEST é uma sigla que significa "Código Especificador da Substituição Tributária". Foi criado para estabelecer uma sistemática de uniformização e identificação das mercadorias e bens que são passíveis de Substituição Tributária e antecipação de ICMS.

O CEST é composto por 7 (sete) dígitos, sendo que:

1. o primeiro e o segundo correspondem ao segmento da mercadoria ou bem;
2. o terceiro ao quinto correspondem ao item de um segmento de mercadoria ou bem;
3. o sexto e o sétimo correspondem à especificação do item.

---

## Como escolher o código CEST

Para cada Código CEST existe um conjunto de códigos NCMs relacionados. Com base na tabela publicada pelo [Convênio ICMS 92/15](https://www.confaz.fazenda.gov.br/legislacao/convenios/2015/CV092_15), nosso sistema irá sugerir os CESTs mais indicados para o NCM do seu produto. Geralmente você precisará apenas escolher o CEST que melhor corresponder a descrição/segmento do produto. Se você continuar em dúvida, sugerimos confirmar qual o CEST ideal para o produto com a sua contabilidade.

Algumas observações importantes:

1. Não existe código CEST para todo código NCM
2. Para cada código NCM podem existir mais de um código CEST
3. Para cada código CEST podem existir mais de um código NCM
4. Nem todo produto precisará ter o código CEST informado

---

# Atualizando NCM e CEST dos produtos vendidos

## Verificando produtos com cadastro incompleto

Para buscar todos os produtos vendidos pelo seu estabelecimento filtrando pelos que possuem cadastro incompleto:

1. Acessar o módulo: **"Administrativo >> Produtos >> Produtos Vendidos"**  
_Obs.: Nessa tela será exibida uma listagem com todos os produtos cadastrado no sistema que estão com a venda habilitada_
2. Clique no botão em exibir filtros no canto direito superior
3. Na coluna status selecione:
   1. _“Cadastro com pendências”_: Para filtrar por produtos que não possuem o campo NCM ou CEST preenchidos
   2. _“Cadastro incompleto”_: Para filtrar por produtos que não possuem campo Unidade ou Preço de Venda preenchidos

![01.png](./res/ncm-cest/01.png)
_Obs.: Passe o mouse sobre o ícone exibido na coluna status para ver os detalhes do status do cadastro do produto._

---

## Alterando o NCM e CEST do produto

1. Após acessar o módulo: **"Administrativo >> Produtos >> Produtos Vendidos"**
2. Selecione o produto que deseja atualizar
3. Clique no ícone de “Editar” ![edit](./res/ncm-cest/edit.png) no canto direito superior
4. No campo “Código NCM” clique no botão ![modal](./res/ncm-cest/modal.png) e selecione o NCM correto para o produto navegando pela árvore de NCMs exibida:
![02.png](./res/ncm-cest/02.png)
5. No campo “Código CEST” clique no botão  e selecione o CEST correto para o produto dentre os códigos CESTs sugeridos para o NCM selecionado:
![03.png](./res/ncm-cest/03.png)
_Obs.: Para selecionar um código CEST diferente do selecionado clique no botão “Todos”   no canto direito acima da lista de códigos CEST sugeridos._

