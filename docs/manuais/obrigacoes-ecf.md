# Obrigações das Empresas Usuárias de ECF

> _[Visualizar versão em PDF](./res/obrigacoes-ecf/obrigacoes-ecf.pdf)_

## Legislação pertinente

Existem rotinas diárias e mensais obrigatórias que devem ser observadas pelo estabelecimento usuário de 
ECF (Emissor de Cupom Fiscal). São elas:

 - Emissão de Redução Z Diária
 - Encerramento de contas abertas
 - Gravação em mídia não regravável de arquivos eletrônicos do ECF

Para entender melhor o funcionamento de aplicativos PAF-ECF e as obrigações das empresas usuárias de
ECF sugerimos a leitura dos manuais 
[Manual Fiscal do Usuário de ECF](http://www.fazenda.mg.gov.br/empresas/ecf/manuais/manu_usu.pdf) e 
[Cartilha do ECF - Perguntas e Respostas](http://www.fazenda.mg.gov.br/empresas/ecf/manuais/cartilha_perg_resp.pdf),
disponibilizados pela [SEF/MG](http://www.fazenda.mg.gov.br/) em
[Manuais - ECF](http://www.fazenda.mg.gov.br/empresas/ecf/_manuais.htm).

---

## Emissão de Redução Z (Diário)

Deve ser emitido o documento Redução Z de todos os equipamentos ECF do estabelecimento utilizados
no dia, no encerramento diário das atividades ou, no caso de funcionamento contínuo do estabelecimento,
até às 24 (vinte e quatro) horas ou até o bloqueio automático do equipamento.

---

## Gravação de Arquivos Eletrônicos (Mensal)

Até o décimo dia útil de cada mês, o usuário de ECF deverá gravar em mídia óptica não regravável (CD ou
DVD) os seguintes arquivos eletrônicos:

 - Arquivos binários da MF e da MFD extraídos dos equipamentos ECF utilizados em pelo menos um
dia do mês.
 - Arquivo texto (TXT), tipo TDM, gerado a partir dos arquivos binários previstos no item anterior,
contendo todos os dados armazenados em todos os dispositivos de memória do ECF, conforme
leiaute estabelecido no Ato COTEPE/ICMS 17/04, de 29 de março de 2004.
 - Arquivo texto (TXT) gerado a partir do Menu Fiscal do PAF-ECF contendo informações
armazenadas no Banco de Dados utilizado pelo PAF-ECF.

Os arquivos eletrônicos gerados, inclusive os arquivos binários extraídos do ECF, devem ser mantidos pelo
estabelecimento usuário do ECF pelo prazo de 5 (cinco) anos, pois são considerados pela legislação
tributária como documentos fiscais sujeitos ao período de prescrição e decadência.

Importante ressaltar que a falta de geração mensal dos arquivos eletrônicos pode trazer graves
consequências ao estabelecimento usuário, pois além de se caracterizar como irregularidade por
descumprimento de obrigação tributária acessória, sujeita a multa, poderá ser necessária a apresentação
destes arquivos em caso de queima ou dano nos dispositivos de memória eletrônica do ECF na ocasião da
cessação de uso ou na solicitação de autorização para substituição da memória, o que impedirá o
deferimento do pedido, caso não seja apresentado.

> Texto retirado do _Manual Fiscal do Usuário de ECF_ disponibilizado pela SEF/MG em
> [Manuais - ECF](http://www.fazenda.mg.gov.br/empresas/ecf/_manuais.htm).

---

## Encerramento de Contas Abertas

Não é permitido que contas de clientes fiquem abertas após a emissão da RZ referente ao dia seguinte a 
data da abertura da Conta de Cliente. Dessa forma, caso o cupom fiscal relativo a uma Conta de Cliente 
ou Mesa não seja emitido até a emissão da Redução Z referente ao movimento do dia seguinte ao do 
registro de abertura da Mesa ou Conta de Cliente, o PAF-ECF irá emitir, automaticamente o Cupom Fiscal 
respectivo, com meio de pagamento “dinheiro”.

Para evitar que o encerramento automático da conta cliente aconteça. O usuário do PAF-ECF deverá observar 
a existência de contas abertas e com data de abertura anteriores ao dia atual. Caso existam, providenciar 
que o seu encerramento seja realizado na forma de pagamento correta antes que a próxima Redução Z seja 
realizada.

---

# Executando obrigações mensais com o IDF PDV

## Gerando arquivo Registros do PAF-ECF

Para gerar o arquivo "Registros do PAF-ECF" e necessário:

1. Acessar o menu: `Menu Fiscal >> Registros do PAF-ECF`
2. Selecionar o período desejado
3. Clicar em Gerar
4. Será exibida uma mensagem informando o diretório onde o arquivo gerado foi salvo no seu computador

![Gerando arquivo Registros do PAF-ECF](./res/obrigacoes-ecf/registrosdopaf.png)

---

## Gerando arquivo MF (Memória Fiscal)

1. Acessar o menu: `Menu Fiscal >> Arq. MF`
2. Selecionar o período desejado
3. Clicar em Gerar
4. Ao final do processo será exibida uma mensagem informando o diretório onde o arquivo foi salvo no seu computador

![Gerando arquivo MF](./res/obrigacoes-ecf/arquivomf.png)

---

## Gerando arquivo MFD (Memória de Fita Detalhe)

1. Acessar o menu: `Menu Fiscal >> Arq. MFD`
2. Selecionar o período desejado
3. Clicar em Gerar
4. Ao final do processo será exibida uma mensagem informando o diretório onde o arquivo foi salvo no seu computador

![Gerando arquivo MFD](./res/obrigacoes-ecf/arquivomfd.png)

---

## Gerando o arquivo TDM

Para gerar o arquivo TDM é necessário utilizar o aplicativo WinMFD2 fornecido pela Bematech.
Verifique se você já possui o programa instalado no seu computador, caso contrário prossiga com o download e instalação do programa.

1. Download e Instalação do Bematech WinMFD2  
_Obs.: Pule esse passo caso você já possua o aplicativo WinMFD2 instalado no seu ponto de venda_
	1. Baixe programa _WinMFD2Setup.exe_ disponível em [www.idfsistemas.com.br/downloads/bematech-winmfd.zip](http://www.idfsistemas.com.br/downloads/bematech-winmfd.zip)
	2. Descompacte o arquivo baixado, será criada a pasta _Bematech-WinMFD2_ que conterá o arquivo _WinMFD.exe_
	3. Execute o arquivo _WinMFD.exe_ para abrir o programa _Bematech WinMFD2_
2. Abra o programa _Bematech WinMFD2_ previamente instalado
3. No menu `Comunicação >> Modelo Impressora`, selecione o modelo _MP-4000 TH FI_
4. Acessar o menu: `Arquivo >> Ato Cotepe 17/04 (Tipo e)`
5. Preencha o formulário exibido com as informações abaixo e clique no botão _Gerar Registros_
	1. **Tipo de Arquivo:** _2 - TDM_
	2. **Arquivo MFD:** _Selecione o arquivo MFD gerado com o IDF PDV_
	3. **Destino:** Não é necessário alterar, será preenchido automaticamente com o local onde o arquivo TDM será gerado
	4. **Data Inicial:** Selecione a data inicial do período (Geralmente o primeiro dia do mês anterior)
	5. **Data Final:** Selecione a data final do período (Geralmente o últiom dia do mês anterior)
	6. **Razão Social:** Deixar em branco
	7. **Endereço:** Deixar em branco
	8. **Versão Ato Cotepe:** _Versão 1_
	9. **Incluir assinatura EAD no arquivo destino:** Deixe marcado

![Gerando o arquivo TDM](./res/obrigacoes-ecf/arquivotdm.png)

---

# Executando obrigações diárias no IDF PDV

## Reduções Z

Redução Z é o documento fiscal emitido no ECF quando do encerramento das atividades diárias do
estabelecimento. Esse documento destina-se à escrituração fiscal do contribuinte e e pode ser
requerido pelo contabilista responsável pela empresa.

---

### Emitindo Redução Z

Para emitir a RZ no IDF PDV basta acessar o menu `MENU FISCAL >> RZ - Redução Z`. A Redução Z deve ser emitida diariamente
e uma vez por Ponto de Venda sempre que houver movimento fiscal no PDV.

 > **ATENÇÃO:** As seguintes operações poderão ser executadas automaticamente imediatamente
 > antes ou imediatamente após a impressão da RZ dependendo do status atual do ECF:
 >  - Impressão do relatório de contas abertas
 >  - Encerramento automático de contas vencidas
 >  - Fechamento de caixa

### Bloqueio por RZ Pendente

Caso a RZ não seja emitida até as 02:00 am do dia seguinte ao dia em que houve impressão de documentos fiscais no ECF, a 
impressora fiscal ficará bloqueada até a emissão da redução Z pendente. Nesse cenário, o IDF PDV exibirá o aviso 
_"ECF Pendente de emissão de RZ"_ no painel de notificações conforme exibido na imagem abaixo:

![ECF Pendente de emissão de RZ](./res/obrigacoes-ecf/rzpendente.png)

### Bloqueio por RZ Emitida

Após a emissão da RZ referente ao movimento fiscal do dia atual o ECF ficará bloqueado e nenhuma operação fiscal poderá ser 
registrado no mesmo até o término do dia atual. Nesse cenário, o IDF PDV exibirá a mensagem 
_"ECF bloqueado por RZ emitida"_ no painel de notificações conforme exibido na imagem abaixo:

![Bloqueio por RZ Emitida](./res/obrigacoes-ecf/rzemitida.png)

---

## Fechamento automático de contas

Não é permitido que uma conta de cliente permaneça aberta por período superior a dois dias após a 
data de venda do seu primeiro item vendido. Quando uma conta se encontra nessa situação ela é
denominada de _Conta Vencida_.

Para atender a esse requisito fiscal, o IDF PDV realiza o encerramento automático de contas vencidas
no momento da emissão da última RZ do estabelecimento referente ao movimento do dia seguinte a data de
venda do primeiro item vendido na conta. Ou no momento da primeira abertura de caixa dois dias posterior
a esta data. Esse encerramento automático da conta será realizado através da emissão do cupom fiscal da
conta com registro de pagamento integral do valor da conta utilizando a forma de pagamento Dinheiro.

Para evitar que contas sejam encerradas automaticamente é **importante que o usuário do PAF-ECF esteja
atento a existência de contas abertas anteriores ao dia atual** e, quando for o caso, providencie o seu
fechamento com a forma de pagamento adequada antes que a próxima RZ seja emitida.
