# Comissões por Garçom

> [Visualizar versão em PDF](./res/comissao-por-garcom/comissao-por-garcom.pdf)

No **IDF Gestão** os valores de comissões geradas pela venda do produto **"Taxa de serviço"** podem ser obtidos de diferentes formas através do relatório de **Comissões por Garçom**


## Executando o relatório

1. Acesse o módulo: Administrativo > Relatórios > Venda
2. Selecione o relatório: Comissões por garçom
3. Escolha o filtro de data do período desejado e clique em: **"Executar relatório"**
4. Será exibido o resultado das comissões detalhados por:
   - Data da venda
   - Garçom que fez a venda
   - Ponto de venda em que o cupom foi fechado


## Agrupando os resultados

Alterando as opções avançadas você pode selecionar quais colunas devem ser utilizadas para agrupar o resultado das comissões. Você poderá agrupar o resultado das comissões por uma ou mais das opções abaixo:

   - Data da venda
   - Garçom que fez a venda
   - Ponto de venda em que o cupom foi fechado

![resultado01.png](./res/comissao-por-gacom/resultado01.png)
_No exemplo acima o resultado foi agrupado somente pelo garçom. Dessa forma foi exibido a comissão de cada garçom no período selecionado somando a comissão de cada um deles: Em todos os PDVs; E em todos os dias em que fizeram vendas._


## Adicionando totalizadores ao relatório

Você ainda poderá totalizar a coluna de comissão do relatório obtido agrupando uma ou mais das demais colunas do relatório:

[resultado02.png](./res/comissao-por-gacom/resultado02.png)
_Nesse exemplo foram adicionados dois totalizadores: Totalizador de Comissão por Garçom; E Totalizador de Comissão por PDV._


## Exportando resultado para Excel

Para exportar todo o conteúdo do relatório para CSV basta clicar no botão **"Todos"** e posteriormente no botão **"Exportar .csv"**

![resultado03.png](./res/comissao-por-gacom/resultado03.png)


## Como é calculada a comissão

1. Todo item vendido em cupons fiscais no sistema possuirá um vendedor que poderá ser:
	1. O garçom que registrou a venda no sistema de pedidos
	2. O operador de caixa que registrou a venda no caixa
2. Ao adicionar a Taxa de Serviço no cupom, o _valor real_* dessa taxa é _dividido_* entre todos os itens do cupom que possuem taxa de serviço aplicável.
	1. *_valor real_: Valor da taxa de serviço considerando descontos/acréscimos aplicados nessa taxa de serviço e descontos/acréscimos aplicados no cupom fiscal.
	2. *_dividido_: Essa divisão é realizada de forma proporcional ao valor de cada item vendido (considerando os descontos/acréscimos dados no item)
3. Por fim, a comissão de cada garçom será igual a soma da taxa de serviço referente a cada _item vendido_* que ele registrou no sistema.
	1. _item vendido_: Itens em cupons cancelados ou itens cancelados em cupom fechados não geram comissão por não serem considerados no cálculo da taxa de serviço

### Exemplo de cálculo p/ um cupom

Considerando o cupom fiscal ao lado onde os itens 01 e 02 foram lançados pelo Garçom 01 (G1) e o item 03 foi lançado pelo Garçom 02 (G2):

![cupom.png](./res/comissao-por-gacom/cupom.png)

1. O desconto de R$ 4,05 dado no cupom é dividido igualmente entre os itens do cupom:
	1. Item 01: 8,00 - (8,00 * 10%) = R$ 7,20
	2. Item 02: CANCELADO
	3. Item 03: (32,00 - 3,20) - ((32,00 - 3,20) * 10%) =  R$ 25,92
	4. Item 04: 3,68 - (3,68 * 10%) = R$ 3,31  
	  Logo o valor real da taxa de serviço do cupom é de R$ 3,31
2. O valor real obtido para a taxa de de serviço é dividido entre os garçons do cupom de forma proporcional:
	1. G1 vendeu R$ 7,20 ⇒ 3,31/(25,92 + 7,20) * 7,20 = R$ 0,72
	2. G2 vendeu R$ 25,92 ⇒  3,31/(25,92 + 7,20) * 25,92 = R$ 2,59
3. Dessa forma, as comissões do cupom serão: 
	1. Garçom 01 ⇒ R$ 0,72
	2. Garçom 02 ⇒ R$ 2,59
